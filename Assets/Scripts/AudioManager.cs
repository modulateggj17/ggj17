﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : SingletonBehaviour<AudioManager>
{

	public enum SoundKeys
	{
		BALL_COLLISION,
		GOAL
	};

	AudioSource inGameMusic;
	AudioSource menuMusic;
	AudioSource winMusic;

	public AudioClip[] Clips = new AudioClip[10];
	

	public void PlaySound(string clipKey, Vector3 position)
	{
		AudioClip selectedClip = null;

		foreach(AudioClip clip in Clips)
		{
			if(clip.name == clipKey)
			{
				selectedClip = clip;
				break;
			}
		}

		print(selectedClip.name);
				
		if (selectedClip != null) 
		{
			StartCoroutine(DoPlay(selectedClip, position));
		}
	}

	IEnumerator DoPlay(AudioClip clip, Vector3 pos)
	{
		GameObject go = new GameObject(clip.name);
		AudioSource source = go.AddComponent<AudioSource>();
		go.transform.position = pos;
		source.clip = clip;
		source.Play();
		yield return new WaitForSeconds(clip.length + 1);
		Destroy(go);
	}

	public void PlayInGameMusic(){
		menuMusic.Stop ();
		inGameMusic.Play ();
	}

	public void PlayWinMusic(){
		inGameMusic.Stop ();
		winMusic.Play ();
	}

	public void PlayMenuMusic(){
		winMusic.Stop ();
		inGameMusic.Stop ();
		menuMusic.Play ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}
}
