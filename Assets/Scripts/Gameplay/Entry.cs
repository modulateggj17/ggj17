﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entry : SingletonBehaviour<Entry>
{
	[SerializeField] Ball ball;

	// Use this for initialization
	void Start ()
	{
		DropBall();	
	}
	
	public void DropBall()
	{
		AudioManager.instance.PlaySound("BallShoot", transform.position);
		Instantiate(ball, transform.position, transform.rotation);
	}

}
