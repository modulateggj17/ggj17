﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	[SerializeField] float destructionHeight = -15;

	[SerializeField] float bounceForce = 30;

	Rigidbody2D body;

	IEnumerator Wait(float waitTime) {
		print (Time.time);
		yield return new WaitForSeconds(waitTime);
		print (Time.time);
	}

	private void Awake()
	{
		body = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update ()
	{
		if(transform.position.y < destructionHeight)
		{
			AudioManager.instance.PlaySound ("BallDrop", transform.position);
			Destroy(gameObject);
			Entry.instance.DropBall();
		}
	}
	
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.gameObject.tag == "Killer")
		{
			Destroy(gameObject);
			Entry.instance.DropBall();
			AudioManager.instance.PlaySound("BallDestroy", transform.position);
		}
		else
		{
			if(collision.collider.CompareTag("Wall"))
				AudioManager.instance.PlaySound("HitWall", transform.position);
			if (collision.collider.CompareTag("Eel"))
				AudioManager.instance.PlaySound("BallLandOnWaveShort", transform.position);
			
			body.velocity = collision.contacts [0].normal * bounceForce;
		}
	}
}
