﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonBehaviour<GameManager>
{
	

	public void EndLevel()
	{
		StartCoroutine(DoEndGame());
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	IEnumerator DoEndGame()
	{
		MusicPlayer.playingPlayer.PlayExtraClip();

		yield return StartCoroutine(LevelMessageUI.instance.ShowMessage("Level Complete"));
		
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
	}
}
