﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveEmitter : MonoBehaviour
{
	[Header("Player Input")]
	[Range(0,1)]
	public float amplitudeInput = 0.5f;
	[Range(0, 1)]
	public float frequencyInput = 0.5f;
	[Range(0, 1)]
	public float placementInput = 0.5f;

	[Header("Gameplay")]
	public float amplitude = 5f;
	public float frequency = 10f;
	public float sampleRate = 0.5f;
	public float phaseSpeed = 3f;
	public float length = 100;
	public float segmentSpeed = 1f;
	public float segmentLength = 1f;

	[Header("Components")]
	[SerializeField] EdgeCollider2D edgeCollider;
	[SerializeField] LineRenderer backgroundLineRenderer;
	[SerializeField] LineRenderer segmentLneRenderer;

	[SerializeField] Transform head;

	[SerializeField] Texture2D[] textures;
	[SerializeField] Sprite[] heads;
	
	[Header("Colors")]
	
	float phase = 0;
	

	void Start ()
	{
		if(gameObject.tag == "Emitter1")
		{
			segmentLneRenderer.material.SetTexture("_MainTex", textures[0]);
			head.GetComponent<SpriteRenderer>().sprite = heads[0];
		}
		else
		{
			segmentLneRenderer.material.SetTexture("_MainTex", textures[1]);
			head.GetComponent<SpriteRenderer>().sprite = heads[1];
		}

	}

	void Update ()
	{
		List<Vector2> points;

		float goingBack = 0;// placementInput < 0.5f ? placementInput * 2 : 0;
		
		phase += (1 - goingBack) * phaseSpeed * Time.deltaTime;
		phase = Mathf.Repeat(phase, Mathf.PI * 2);

		GeneratePoints(0, length, Vector2.zero, out points);

		Vector3[] points3D = new Vector3[points.Count];
		for (int i = 0; i < points.Count; i++)
			points3D[i] = points[i];

		backgroundLineRenderer.numPositions = points3D.Length;
		backgroundLineRenderer.SetPositions(points3D);

		float start = placementInput * (length - segmentLength);
		float end = start + segmentLength;
				
		GeneratePoints(start, end, Vector2.zero, out points);
		edgeCollider.points = points.ToArray();

		points3D = new Vector3[points.Count];
		for (int i = 0; i < points.Count; i++)
			points3D[i] = points[i];

		segmentLneRenderer.numPositions = points3D.Length;
		segmentLneRenderer.SetPositions(points3D);

		Vector2 lastPoint = points[points.Count - 1];
		Vector2 secondLastPoint = points[points.Count - 2];

		Vector2 direction = lastPoint - secondLastPoint;
		head.localPosition = lastPoint;
		head.right = direction;

		//if (Input.GetKeyDown(KeyCode.Space))
		//{
		//	StartSegment();
		//}
	} 

	//public void StartSegment()
	//{
	//	StopAllCoroutines();
	//	StartCoroutine(TransmitSegment());
	//}

	void GeneratePoints(float start, float end, Vector2 offset, out List<Vector2> points)
	{
		points = new List<Vector2>();

		if (sampleRate < 0.0001f)
			return;

		Vector2 right = transform.right;
		Vector2 up = transform.up;
				
		for(float x = start; x < end; x += sampleRate)
		{
			float y = Sample(x);

			points.Add(right * x + up * y + offset);
		}					
	}

	float Sample(float t)
	{
		float x = Mathf.Sin(t * frequency * frequencyInput - phase) * amplitude * amplitudeInput;
		return x;
	}

	IEnumerator TransmitSegment()
	{
		segmentLneRenderer.gameObject.SetActive(true);

		for (float t = -segmentLength; t < length; t += Time.deltaTime * segmentSpeed)
		{
			List<Vector2> points;
			
			GeneratePoints(t, t + segmentLength, Vector2.zero, out points);
			edgeCollider.points = points.ToArray();

			Vector3[] points3D = new Vector3[points.Count];
			for (int i = 0; i < points.Count; i++)
				points3D[i] = points[i];

			segmentLneRenderer.numPositions = points3D.Length;
			segmentLneRenderer.SetPositions(points3D);
			
			yield return 0;
		}

		segmentLneRenderer.gameObject.SetActive(false);
	}
}
