﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
	[SerializeField] AudioSource source;

	[SerializeField] AudioClip startClip;
	[SerializeField] AudioClip repeatClip;

	[SerializeField] AudioClip extraClip;

	public static MusicPlayer playingPlayer;

	// Use this for initialization
	void Awake()
	{
		if( playingPlayer == null)
		{
			playingPlayer = this;
			StartCoroutine(Play());
		}
		else
		{
			if(playingPlayer.name != this.name)
			{
				Destroy(playingPlayer.gameObject);
				playingPlayer = this;
				StartCoroutine(Play());
			}
			else
				Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}

	public void PlayExtraClip()
	{
		StartCoroutine(PlayExtra());
	}

	IEnumerator PlayExtra()
	{
		StopAllCoroutines();
		source.Stop();
		source.clip = extraClip;
		source.Play();

		yield return new WaitForSeconds(startClip.length);

		source.clip = repeatClip;
		source.loop = true;
		source.Play();
	}

	IEnumerator Play()
	{
		source.clip = startClip;
		source.loop = false;
		source.Play();

		yield return new WaitForSeconds(startClip.length);
		
		source.clip = repeatClip;
		source.loop = true;
		source.Play();		
	}
}
