﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
	public const float ROTATION_SPEED = 120.0f;

	public GameObject ExitSprite;

	void Update()
	{
		if (ExitSprite != null)
		{
			ExitSprite.transform.Rotate(0, 0, ROTATION_SPEED * Time.deltaTime);
		}
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		
		if(other.CompareTag("Ball"))
		{
			AudioManager.instance.PlaySound ("Black Hole", transform.position);
			Destroy(other.gameObject);
			GameManager.instance.EndLevel();
		}
	}
	

}
