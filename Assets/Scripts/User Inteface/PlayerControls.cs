﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerControls : MonoBehaviour
{
	#region Constants

	public const float CONTROLLER_DEADZONE_THRESHOLD = 0.8f;

	#endregion

	#region Fields and Properties

	private static Dictionary<int, int> controllerAssignments;
	public static Dictionary<int, int> ControllerAssignments
	{
		get
		{
			if (controllerAssignments == null)
			{
				controllerAssignments = new Dictionary<int, int>();
			}
			return controllerAssignments;
		}
		private set
		{
			controllerAssignments = value;
		}
	}

	public string WaveEmitterTag;
	public int PlayerId;
	private string controllerName;

	public DialControl AmplitudeDial;
	public DialControl FrequencyDial;
	public DialControl PlacementDial;

	private WaveEmitter waveEmitter;

	private DialControl.WaveProperty selectedWaveProperty;

	private Vector2 lastNormalizedDirectionLeftStick;
	private Vector2 lastNormalizedDirectionRightStick;

	#endregion

	#region Lifecycle

	protected void Awake()
	{
	}

	// Use this for initialization
	protected void Start()
	{
		AssignControllerForPlayer();

		if (WaveEmitterTag != null && WaveEmitterTag.Length > 0) {
			GameObject waveEmitterGO = GameObject.FindGameObjectWithTag(WaveEmitterTag);
			if (waveEmitterGO != null) {
				waveEmitter = waveEmitterGO.GetComponent<WaveEmitter>();
			}

			if (waveEmitter != null && AmplitudeDial != null) {
				AmplitudeDial.ConnectToWaveEmitter(waveEmitter);
				AmplitudeDial.Reset();
			}
			if (waveEmitter != null && FrequencyDial != null) {
				FrequencyDial.ConnectToWaveEmitter(waveEmitter);
				FrequencyDial.Reset();
			}
			if (waveEmitter != null && PlacementDial != null) {
				PlacementDial.ConnectToWaveEmitter(waveEmitter);
				PlacementDial.Reset();
			}
		} 
		else 
		{
			gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	protected void Update()
	{
		HandleControllerInput();
	}

	#endregion

	#region Controller Input

	private void AssignControllerForPlayer()
	{
//		string[] controllerNames = Input.GetJoystickNames();
//		for (int i = 0; i < controllerNames.Length; i++) 
//		{
//			if (controllerNames[i].Length > 0)
//			{
//				
//			}
//		}
	}

	private void HandleControllerInput()
	{
		string[] joystickNames = Input.GetJoystickNames();
		int controllerCount = joystickNames.Length;
		int offset = 0;
		for (int i = 0; i < joystickNames.Length; i++)
		{
			if (joystickNames [i].Length <= 0)
			{
				offset++;
			}
		}
		if (PlayerId > 0 && PlayerId < 5 && 
			gameObject.activeSelf && controllerCount >= (PlayerId+offset)) 
		{
			string playerIdStr = (PlayerId + offset).ToString();

			// Left Stick
			float xAxisLeft = Input.GetAxis("L_XAxis_" + playerIdStr);
			float yAxisLeft = Input.GetAxis("L_YAxis_" + playerIdStr);
			Vector2 rawDirectionLeftStick = new Vector2(xAxisLeft, yAxisLeft);
			Vector2 normalizedDirectionLeftStick = (new Vector2(xAxisLeft, yAxisLeft)).normalized;
			if ((xAxisLeft != 0 || yAxisLeft != 0) &&
				(Mathf.Abs(rawDirectionLeftStick.x) > CONTROLLER_DEADZONE_THRESHOLD ||
					Mathf.Abs(rawDirectionLeftStick.y) > CONTROLLER_DEADZONE_THRESHOLD) &&
			    normalizedDirectionLeftStick != lastNormalizedDirectionLeftStick) {
				AmplitudeDial.SetValueByDirection(normalizedDirectionLeftStick);
			}
			lastNormalizedDirectionLeftStick = normalizedDirectionLeftStick;

			// Right Stick
			float xAxisRight = Input.GetAxis("R_XAxis_" + playerIdStr);
			float yAxisRight = Input.GetAxis("R_YAxis_" + playerIdStr);
			Vector2 rawDirectionRightStick = new Vector2(xAxisRight, yAxisRight);
			Vector2 normalizedDirectionRightStick = (new Vector2(xAxisRight, yAxisRight)).normalized;
			if ((xAxisRight != 0 || yAxisRight != 0) &&
				(Mathf.Abs(rawDirectionRightStick.x) > CONTROLLER_DEADZONE_THRESHOLD ||
					Mathf.Abs(rawDirectionRightStick.y) > CONTROLLER_DEADZONE_THRESHOLD) &&
			    normalizedDirectionRightStick != lastNormalizedDirectionRightStick) 
			{
				FrequencyDial.SetValueByDirection(normalizedDirectionRightStick);
			}
			lastNormalizedDirectionRightStick = normalizedDirectionRightStick;

			// Triggers
			float triggerAxisLeft = Input.GetAxis("TriggersL_" + playerIdStr);
			float triggerAxisRight = Input.GetAxis("TriggersR_" + playerIdStr);
			if (triggerAxisLeft > 0) 
			{
				PlacementDial.DecreaseValueByStep(triggerAxisLeft);
			} 
			else if (triggerAxisRight > 0) 
			{
				PlacementDial.IncreaseValueByStep(triggerAxisRight);
			} 
		}
	}

	#endregion
}

