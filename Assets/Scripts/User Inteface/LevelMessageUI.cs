﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMessageUI : SingletonBehaviour<LevelMessageUI>
{
	[SerializeField] UnityEngine.UI.Text text;
	[SerializeField] float duration = 3f;

	private void Awake()
	{
		base.Awake();
		text.enabled = false;
	}

	public IEnumerator ShowMessage(string message)
	{
		text.text = message;
		text.enabled = true;

		print(message);

		yield return new WaitForSeconds(duration);

		print(message);
		text.enabled = false;
	}
}
