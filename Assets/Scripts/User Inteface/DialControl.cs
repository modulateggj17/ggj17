﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialControl : MonoBehaviour, IDragHandler
{
	#region Constants

	public const float HALF_ROTATION_THRESHOLD = 135.0f;
	public const float MIN_ROTATION = 360.0f - HALF_ROTATION_THRESHOLD;
	public const float MAX_ROTATION = HALF_ROTATION_THRESHOLD;
	public const float INC_DEC_SMOOTHING_TIME = 0.05f;
	public const float ANGLE_SMOOTHING_TIME = 0.8f;

	public const float STEP_AMOUNT = 0.025f;

	private float previousAngle = 0;
	private float dampVel = 0;

	#endregion

	#region Fields and Properties

	public enum WaveProperty
	{
		Frequency,
		Amplitude,
		Placement
	};

	public float CurrentValue { get; set; }

	public string Title;
	public WaveProperty LinkedWaveProperty;

	public Image DialImage;

	private WaveEmitter waveEmitter;

	#endregion

	#region Lifecycle

	protected void Awake()
	{
		CurrentValue = 0;

		if (LinkedWaveProperty == WaveProperty.Placement)
		{
			DialImage.gameObject.SetActive(false);
		}
	}

	// Use this for initialization
	protected void Start()
	{
		UpdateDialRotation();
	}

	// Update is called once per frame
	protected void Update()
	{

	}

	public void ConnectToWaveEmitter(WaveEmitter waveEmitter)
	{
		this.waveEmitter = waveEmitter;
	}

	#endregion

	#region Handle Input

	public void OnBeginDrag(PointerEventData eventData)
	{

	}

	public void OnDrag(PointerEventData data)
	{
		Vector2 normalizedDelta = data.delta.normalized;

		float change = 0;
		if (normalizedDelta.x > 0)
		{
			change = STEP_AMOUNT;
		}
		else if (normalizedDelta.x < 0)
		{
			change -= STEP_AMOUNT;
		}

		CurrentValue = Mathf.Clamp(CurrentValue + change, 0, 1.0f);
		UpdateForNewValue(true);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
	}

	public void IncreaseValueByStep(float modifier)
	{
		float modifiedStep = (STEP_AMOUNT * modifier);
		float newValue = (CurrentValue + modifiedStep);
		newValue = Mathf.SmoothDamp(CurrentValue, newValue, ref dampVel, INC_DEC_SMOOTHING_TIME);
		CurrentValue = Mathf.Clamp(newValue, 0, 1.0f);
		UpdateForNewValue(true);
	}

	public void DecreaseValueByStep(float modifier)
	{
		float modifiedStep = (STEP_AMOUNT * modifier);
		float newValue = (CurrentValue - modifiedStep);
		newValue = Mathf.SmoothDamp(CurrentValue, newValue, ref dampVel, INC_DEC_SMOOTHING_TIME);
		CurrentValue = Mathf.Clamp(newValue, 0, 1.0f);
		UpdateForNewValue(true);
	}

	private void UpdateForNewValue(bool updateRotation)
	{
		if (updateRotation)
		{
			UpdateDialRotation();
		}

		if (waveEmitter != null) 
		{
			switch (LinkedWaveProperty) 
			{
			case WaveProperty.Frequency:
				waveEmitter.frequencyInput = CurrentValue;
				break;
			case WaveProperty.Amplitude:
				waveEmitter.amplitudeInput = CurrentValue;
				break;
			case WaveProperty.Placement:
				waveEmitter.placementInput = CurrentValue;
				break;
			}
		}
	}

	public void SetValueByDirection(Vector2 direction)
	{
		float angle = NormalAbsoluteAngleDegrees(
			((Mathf.PI/2.0f) + Mathf.Atan2(direction.y, direction.x)) * Mathf.Rad2Deg);

		float smoothedAngle = Mathf.SmoothDampAngle(angle, previousAngle, ref dampVel, ANGLE_SMOOTHING_TIME);
		previousAngle = smoothedAngle;

		float adjustedAngle = NormalAbsoluteAngleDegrees(smoothedAngle + MAX_ROTATION);

		if (!(angle <= MIN_ROTATION && angle >= MAX_ROTATION)) 
		{
			DialImage.transform.localRotation = Quaternion.AngleAxis(-smoothedAngle, Vector3.forward);

			float rotationRange = NormalAbsoluteAngleDegrees(MAX_ROTATION - MIN_ROTATION);

			float percentage = (adjustedAngle / rotationRange);

			CurrentValue = Mathf.Clamp(percentage, 0, 1.0f);

			UpdateForNewValue(false);
		}
	}

	public float NormalAbsoluteAngleDegrees(float angle) 
	{
		return (angle %= 360) >= 0 ? angle : (angle + 360);
	}

	private void UpdateDialRotation()
	{
		float rotationRange = NormalAbsoluteAngleDegrees(MAX_ROTATION - MIN_ROTATION);
		float newRotation =  NormalAbsoluteAngleDegrees(MIN_ROTATION + (rotationRange * (1.0f - CurrentValue)));

		DialImage.transform.localRotation = Quaternion.AngleAxis(newRotation, Vector3.forward);
	}

	public void Reset()
	{
		CurrentValue = 0;
		UpdateForNewValue(true);
	}

	#endregion
}

