﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToScene : MonoBehaviour
{

	public string sceneName;
		
	// Update is called once per frame
	public void OnCLick()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
		AudioManager.instance.PlaySound ("CassetteDeckSwitch", transform.position);
	}
}
