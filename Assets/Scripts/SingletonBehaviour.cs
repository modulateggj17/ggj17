﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Lazy singleton implementation that requires a human adds it to each scene it is necessary in and that it is on it's own game object/prefab.
/// This is to facilitate easier collaboration since each prefab/singleton can be edited as a single unit
/// </summary>
public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
	public static T instance
	{
		get;
		private set;
	}

	protected virtual void Awake()
	{
		if (instance == null)
		{
			instance = this as T;
		}
		else
		{
			Destroy(gameObject);
		}
	}

}
