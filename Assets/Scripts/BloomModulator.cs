﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloomModulator : MonoBehaviour
{
	[SerializeField] AnimationCurve modulatorCurve;

	UnityStandardAssets.ImageEffects.BloomOptimized bloom;

	void Awake()
	{
		bloom = GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>();
	}

	public void Update()
	{
		bloom.intensity = modulatorCurve.Evaluate(Time.time);
	}		
}
